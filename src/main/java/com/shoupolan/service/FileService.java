package com.shoupolan.service;

import com.shoupolan.common.bean.FileBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @program: fxspg
 * @description:
 * @author: youjp
 * @create: 2019-06-21 11:05
 **/
@Service
public class FileService {

    @Value("${image.uploadPath}")
    private String imageUploadPath;

    public List<FileBean> upload(HttpServletRequest request, MultipartFile[] files) {
        List<FileBean> filePath = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        try {
            for (MultipartFile file : files) {
                //todo 前端传递业务类型。判断能否上传
                String originalFilename = file.getOriginalFilename();
                String type = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."),file.getOriginalFilename().length());
                String fullFileName = System.currentTimeMillis() + type;
                String serveFileName = fullFileName.substring(0,fullFileName.lastIndexOf("."));
                String originalName = file.getOriginalFilename().substring(0, file.getOriginalFilename().lastIndexOf("."));
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH) + 1;
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                String savePath = imageUploadPath + "/"+ year + "/" + month + "/"+ day + "/" + fullFileName;
                File target = new File(savePath);
                if (!target.getParentFile().exists()) {
                    target.getParentFile().mkdirs();
                }
                file.transferTo(target);
                //信息封装
                savePath =  "/"+ year + "/" + month + "/"+ day + "/" + fullFileName;

                FileBean fileBean = new FileBean(serveFileName,originalName,savePath,type);
                //返回文件在服务器的地址
                filePath.add(fileBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }

    public void readFile(String fileName, String folder, HttpServletRequest request, HttpServletResponse response) {

        try {
            response.setContentType("image/jpeg");
            //发头控制浏览器不要缓存
            response.setDateHeader("expries", -1);
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            String root_path = imageUploadPath + File.separator + folder + File.separator + fileName;

            ServletOutputStream outputStream = response.getOutputStream();
            FileInputStream fileInputStream = new FileInputStream(new File(root_path));
            BufferedInputStream bufferedInputStream = new BufferedInputStream(
                    fileInputStream);
            byte[] b = new byte[bufferedInputStream.available()];
            bufferedInputStream.read(b);
            outputStream.write(b);

            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void readFile(String url, HttpServletRequest request, HttpServletResponse response) {

        try {
            response.setContentType("image/jpeg");
            //发头控制浏览器不要缓存
            response.setDateHeader("expries", -1);
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            String root_path = imageUploadPath + File.separator + url;

            ServletOutputStream outputStream = response.getOutputStream();
            FileInputStream fileInputStream = new FileInputStream(new File(root_path));
            BufferedInputStream bufferedInputStream = new BufferedInputStream(
                    fileInputStream);
            byte[] b = new byte[bufferedInputStream.available()];
            bufferedInputStream.read(b);
            outputStream.write(b);

            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
