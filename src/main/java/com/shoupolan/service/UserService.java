package com.shoupolan.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shoupolan.common.core.PageData;
import com.shoupolan.error.BusinessException;
import com.shoupolan.error.EmBusinessError;
import com.shoupolan.mapper.UserMapper;
import com.shoupolan.pojo.User;
import com.shoupolan.pojo.UserExample;
import com.shoupolan.pojo.vo.UserVo;
import com.shoupolan.utils.PwdUtil;
import com.shoupolan.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @ClassName:
 * @PackageName: com.shoupolan.service
 * @author: youjp
 * @create: 2019-08-16 21:14
 * @description: TODO 用户业务层
 * @Version: 1.0
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class UserService {

    @Autowired
    UserMapper userMapper;

    /**
     * @return void
     * @Param [user]
     * @Author youjp
     * @Description //TODO 添加用户
     * @throw
     **/
    public void add(User user)  throws BusinessException {
        if(user == null){
            throw new BusinessException(EmBusinessError.USER_NOT_EXIT);
        }
        //判断用户唯一性
        if(userMapper.selectSelctive(user)!=null){
            throw new BusinessException(EmBusinessError.USER_IS_EXIT);
        }
        user.setId(UUIDUtils.generator());
        user.setPassword(PwdUtil.SHA(user.getPassword()));
        userMapper.insertSelective(user);

    }


    /**
     * @return void
     * @Param [user]
     * @Author youjp
     * @Description //TODO 修改用户
     * @throw
     **/
    public void update(User user) {
        userMapper.updateByPrimaryKey(user);
    }

    /**
     * @return void
     * @Param [id]
     * @Author youjp
     * @Description //TODO 删除用户
     * @throw
     **/
    public void delete(String id) {
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andIdEqualTo(id);
        List<User> userList = userMapper.selectByExample(userExample);
        for (User user : userList) {
            user.setState(false);
            userMapper.updateByPrimaryKey(user);
        }
    }

    /**
     * @Param [id]
     * @return com.shoupolan.pojo.User
     * @Author youjp
     * @Description //TODO 获取用户信息
     * @throw
     **/
    public User get(String id){
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andIdEqualTo(id);
        List<User> userList = userMapper.selectByExample(userExample);
         if(CollectionUtils.isEmpty(userList)){
             return null;
         }
         return userList.get(0);
    }

    /**
     * @return com.shoupolan.pojo.User
     * @Param [id]
     * @Author youjp
     * @Description //TODO  获取用户信息
     * @throw
     **/
    public User getUser(String id) {
        return userMapper.selectByPrimaryKey(id);
    }


    public User validateLogin(User vo) {
        //通过用户名拿到用户，对比密码
        User getUser = userMapper.selectSelctive(vo);
        String passwd = getUser.getPassword();
        String nowPawd = PwdUtil.SHA(vo.getPassword());

        if (passwd.equals(nowPawd)) {
            return getUser;
        }
        return null;
    }

    public List<User> getList(UserVo vo) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria= example.createCriteria();
        criteria.andStateEqualTo(true);
        List<User> list = userMapper.selectByExample(example);
        return list;
    }

    public PageData<User> getPage(UserVo vo) {
        Page page = PageHelper.startPage(vo.getPageNo(), vo.getPageSize());
        List<User> userList = userMapper.listPage(vo);
        return new PageData<>(vo.getPageNo(), vo.getPageSize(), page.getTotal(), userList);
    }
}
