package com.shoupolan.service;


import com.shoupolan.common.core.PageData;
import com.shoupolan.mapper.OrderMapper;


import com.shoupolan.pojo.Order;
import com.shoupolan.pojo.OrderExample;
import com.shoupolan.pojo.dto.OrderDto;
import com.shoupolan.pojo.vo.DomainVo;
import com.shoupolan.utils.UUIDUtils;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @ClassName:
 * @PackageName: com.shoupolan.service
 * @author: youjp
 * @create: 2019-08-16 21:13
 * @description:
 * @Version: 1.0    TODO 订单业务层
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class OrderService {

    @Autowired
    OrderMapper orderMapper;

    /**
     * @Param [order]
     * @return void
     * @Author youjp
     * @Description //TODO 添加订单
     * @throw
     **/
    public void add(Order order) {
        order.setId(UUIDUtils.generator());
        orderMapper.insertSelective(order);
    }


    public List<OrderDto> list(){

        //
        return null;
    }

    /**
     * @Param [vo]
     * @return com.github.pagehelper.Page<com.shoupolan.pojo.Order>
     * @Author youjp
     * @Description //TODO 分页
     * @throw
     **/
    @Transactional(readOnly = true)
    public PageData<Order> listPage(DomainVo vo){
  
        return null;
    }
    /**
     * @Param [order]
     * @return void
     * @Author youjp
     * @Description //TODO 修改域名
     * @throw
     **/
    public void update(Order order){
        

    }

    /**
     * @Param [order]
     * @return void
     * @Author youjp
     * @Description //TODO 删除订单
     * @throw
     **/
    public void delete(String id){
        OrderExample domainExample=new OrderExample();
        OrderExample.Criteria criteria=domainExample.createCriteria();
        criteria.andIdEqualTo(id);
        List<Order>  domainList=orderMapper.selectByExample(domainExample);
        for(Order order:domainList){
            order.setState(false);
            orderMapper.updateByPrimaryKeySelective(order);
        }
    }

    /**
     * @Param [id]
     * @return com.shoupolan.pojo.Order
     * @Author youjp
     * @Description //TODO 获取订单详情
     * @throw
     **/
    public Order get(String id){
        OrderExample domainExample=new OrderExample();
        OrderExample.Criteria criteria=domainExample.createCriteria();
        criteria.andIdEqualTo(id);
        List<Order>  domainList=orderMapper.selectByExample(domainExample);
        if (CollectionUtils.isEmpty(domainList)){
            return null;
        }
        return domainList.get(0);
    }
}
