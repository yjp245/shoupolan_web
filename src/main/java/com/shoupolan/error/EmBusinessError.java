package com.shoupolan.error;

/**
 * 枚举对象
 */
public enum EmBusinessError implements CommonError{
    //10001开头为通用信息错误
    PARAMETER_VALIDATION_ERROR(10001, "参数不合法"),
    UNKNOWN_ERROR(10002, "未知错误"),
    //20000开头为用户信息错误
    USER_NOT_EXIT(20001,"用户不存在"),
    USER_LOGIN_FAIL(20002,"用户手机号或密码不正确"),
    USER_IS_EXIT(20003,"用户已经存在"),
    USER_NOT_LOGIN(20004,"用户未登录"),
    USER_ERROR_PASSWORD(20005,"登录密码错误"),

    //30000开头为交易信息错误
    STOCK_NOT_ENOUGH(30001,"库存不足"),
    ;
  private int errCode;
  private String errMsg;

    EmBusinessError(){}
    EmBusinessError(int errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    @Override
    public int getErrCode() {
        return errCode;
    }

    @Override
    public String getErrMsg() {
        return errMsg;
    }

    @Override
    public CommonError setErrorMsg(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }
}
