package com.shoupolan.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.shoupolan.common.core.PageData;
import com.shoupolan.mapper.GoodMapper;
import com.shoupolan.pojo.Domain;
import com.shoupolan.pojo.Good;
import com.shoupolan.pojo.GoodExample;
import com.shoupolan.pojo.dto.GoodDto;
import com.shoupolan.pojo.vo.GoodVo;
import com.shoupolan.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @ClassName:
 * @PackageName: com.shoupolan.service
 * @author: youjp
 * @create: 2019-08-16 21:13
 * @description:    TODO 商品业务层
 * @Version: 1.0
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class GoodService {

    @Autowired
    GoodMapper goodMapper;

    /**
     * @Param [good]
     * @return void
     * @Author youjp
     * @Description //TODO 添加商品
     * @throw
     **/
    public void add(Good good){
        good.setId(UUIDUtils.generator());
        goodMapper.insertSelective(good);
    }

    /**
     * @Param [good]
     * @return void
     * @Author youjp
     * @Description //TODO 修改商品
     * @throw
     **/
    public void update(Good good){
        goodMapper.updateByPrimaryKeySelective(good);
    }


    /**
     * @Param [id]
     * @return void
     * @Author youjp
     * @Description //TODO 删除商品
     * @throw
     **/
    public void delete(String id){
        GoodExample goodExample=new GoodExample();
        GoodExample.Criteria criteria=goodExample.createCriteria();
        criteria.andIdEqualTo(id);
        List<Good> domainList=goodMapper.selectByExample(goodExample);
        for(Good good:domainList){
           good.setState(false);
           goodMapper.updateByPrimaryKeySelective(good);
        }
    }

    /**
     * @Param [id]
     * @return com.shoupolan.pojo.Good
     * @Author youjp
     * @Description //TODO 获取商品详情
     * @throw
     **/
    public GoodDto get(String id){
       return goodMapper.getGoodInfo(id);
    }

    /**
     * @Param []
     * @return java.util.List<com.shoupolan.pojo.Good>
     * @Author youjp
     * @Description //TODO 查询商品列表
     * @throw
     **/
    @Transactional(readOnly = true)
    public List<GoodDto> list(GoodVo vo){
        List<GoodDto> domainList=goodMapper.listPage(vo);
        return  domainList;
    }

    /**
     * @Param [vo]
     * @return com.shoupolan.common.core.PageData<com.shoupolan.pojo.Good>
     * @Author youjp
     * @Description //TODO 分页查询商品列表
     * @throw
     **/
    @Transactional(readOnly = true)
    public PageData<GoodDto> listPage(GoodVo vo){
        Page page = PageHelper.startPage(vo.getPageNo(), vo.getPageSize());
        List<GoodDto> domainList=goodMapper.listPage(vo);
        return new PageData<>(vo.getPageNo(), vo.getPageSize(), page.getTotal(), domainList);
    }
}
