package com.shoupolan.service;

import com.github.pagehelper.PageHelper;
import com.shoupolan.common.core.PageData;
import com.shoupolan.mapper.DomainMapper;
import com.shoupolan.pojo.Domain;
import com.shoupolan.pojo.DomainExample;
import com.shoupolan.pojo.vo.DomainVo;
import com.shoupolan.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @ClassName:
 * @PackageName: com.shoupolan.service
 * @author: youjp
 * @create: 2019-08-16 21:11
 * @description:   TODO 域名业务层
 * @Version: 1.0
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class DomainService {

    @Autowired
    DomainMapper domainMapper;


    /**
     * @Param [domain]
     * @return void
     * @Author youjp
     * @Description //TODO 添加域名
     * @throw
     **/
    public void add(Domain domain) {
        domain.setId(UUIDUtils.generator());
        domainMapper.insertSelective(domain);
    }

    /**
     * @Param []
     * @return java.util.List<com.shoupolan.pojo.Domain>
     * @Author youjp
     * @Description //TODO 查询所有域名
     * @throw
     **/
    public List<Domain> list(){
        DomainExample domainExample=new DomainExample();
        DomainExample.Criteria criteria=domainExample.createCriteria();
        criteria.andStateEqualTo(true);
       List<Domain>  domainList=domainMapper.selectByExample(domainExample);
       return domainList;
    }

    /**
     * @Param [vo]
     * @return com.github.pagehelper.Page<com.shoupolan.pojo.Domain>
     * @Author youjp
     * @Description //TODO 分页
     * @throw
     **/
    @Transactional(readOnly = true)
    public PageData<Domain> listPage(DomainVo vo){
        Page page = PageHelper.startPage(vo.getPageNo(), vo.getPageSize());
        List<Domain>  domainList=domainMapper.listPage(vo);
        return new PageData<>(vo.getPageNo(), vo.getPageSize(),page.getTotal(), domainList);
    }
    /**
     * @Param [domain]
     * @return void
     * @Author youjp
     * @Description //TODO 修改域名
     * @throw
     **/
    public void update(Domain domain){
        domainMapper.updateByPrimaryKeySelective(domain);
    }

    /**
     * @Param [domain]
     * @return void
     * @Author youjp
     * @Description //TODO 删除域名
     * @throw
     **/
    public void delete(String id){
        DomainExample domainExample=new DomainExample();
        DomainExample.Criteria criteria=domainExample.createCriteria();
        criteria.andIdEqualTo(id);
        List<Domain>  domainList=domainMapper.selectByExample(domainExample);
        for(Domain domain:domainList){
            domain.setState(false);
            domainMapper.updateByPrimaryKeySelective(domain);
        }
    }

    /**
     * @Param [id]
     * @return com.shoupolan.pojo.Domain
     * @Author youjp
     * @Description //TODO 获取域名
     * @throw
     **/
    public Domain get(String id){
        DomainExample domainExample=new DomainExample();
        DomainExample.Criteria criteria=domainExample.createCriteria();
        criteria.andIdEqualTo(id);
        List<Domain>  domainList=domainMapper.selectByExample(domainExample);
        if (CollectionUtils.isEmpty(domainList)){
            return null;
        }
        return domainList.get(0);
    }
}
