package com.shoupolan.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * @Param
 * @return
 * @Author youjp
 * @Description //TODO 图片回显
 * @throw
 **/
@RestController
public class ReadImageController {

    @Value("${image.uploadPath}")
    private String imageUploadPath;

    /**
     * @description 获取图片
     * @return
     * @author zl
     * @date 2019-06-29
     * @Param url 图片url，type图片类型
     */
    @GetMapping("readImage")
    public void readImage(HttpServletRequest req, HttpServletResponse resp, @RequestParam("url") String url) throws IOException {
//        resp.setContentType("image/jpg");
////        OutputStream os = resp.getOutputStream();
////        String savePath="";
////        savePath = imageUploadPath+url;
////        InputStream is = new FileInputStream(new File(savePath));
////        this.copy(is, os);
        if ("".equals(url)){
            return ;
        }
        String endPrix = url.substring(url.lastIndexOf("."), url.length());

        try {
            if(".jpg".equals(endPrix) || ".jpeg".equals(endPrix)){
                resp.setContentType("image/jpeg");
            }else{
                resp.setContentType("image/png");
            }
            //发头控制浏览器不要缓存
            resp.setDateHeader("expries", -1);
            resp.setHeader("Cache-Control", "no-cache");
            resp.setHeader("Pragma", "no-cache");
            String root_path = imageUploadPath + File.separator + url;

            ServletOutputStream outputStream = resp.getOutputStream();
            FileInputStream fileInputStream = new FileInputStream(new File(root_path));
            BufferedInputStream bufferedInputStream = new BufferedInputStream(
                    fileInputStream);
            byte[] b = new byte[bufferedInputStream.available()];
            bufferedInputStream.read(b);
            outputStream.write(b);

            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @description 转io流工具方法
     * @return
     * @author zl
     * @date 2019-06-29
     * @Param in out
     */
    public  int copy(InputStream in, OutputStream out) throws IOException {
        try {
            int byteCount = 0;
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
                byteCount += bytesRead;
            }
            out.flush();
            return byteCount;
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
            }
            try {
                out.close();
            } catch (IOException ex) {
            }
        }
    }
}
