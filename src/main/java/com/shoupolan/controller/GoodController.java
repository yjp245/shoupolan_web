package com.shoupolan.controller;

import com.shoupolan.common.bean.FileBean;
import com.shoupolan.common.core.Result;
import com.shoupolan.pojo.Good;
import com.shoupolan.pojo.vo.GoodVo;
import com.shoupolan.service.FileService;
import com.shoupolan.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @ClassName:
 * @PackageName: com.shoupolan.controller
 * @author: youjp
 * @create: 2019-08-16 21:09
 * @description:    TODO
 * @Version: 1.0    商品控制层
 */
@RestController
@RequestMapping("good")
public class GoodController {

    @Autowired
    GoodService goodService;

    @Autowired
    private FileService fileService;

    /**
     * @Param [good]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 添加商品
     * @throw
     **/
    @PostMapping("add")
    public Result add(@RequestBody Good good){
        goodService.add(good);
        return Result.success();
    }

    /**
     * @Param [good]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 修改商品
     * @throw
     **/
    @PutMapping("edit")
    public Result update(@RequestBody Good good){
        goodService.update(good);
        return Result.success();
    }

    /**
     * @Param [id]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 删除商品
     * @throw
     **/
    @DeleteMapping("delete")
    public Result delete(@RequestParam("id") String id){
        goodService.delete(id);
        return Result.success();
    }

    /**
     * @Param [id]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 获取商品详情
     * @throw
     **/
    @ResponseBody
    @GetMapping("/get")
    public Result get(@RequestParam("id") String id){
        return Result.success(goodService.get(id));
    }

    /**
     * @Param []
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 获取商品列表
     * @throw
     **/
    @GetMapping("list")
    public Result list(@ModelAttribute GoodVo goodVo){
        return Result.success(goodService.list(goodVo));
    }

    /**
     * @Param [goodVo]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 分页查询
     * @throw
     **/
    @GetMapping("listPage")
    public Result listPage(@ModelAttribute GoodVo goodVo){
        return Result.success(goodService.listPage(goodVo));
    }


    /**
     * @Param [file, request]
     * @return com.hrt.framework.web.core.Result
     * @Author youjp
     * @Description //TODO 图片上传
     * @throw
     **/
    @PostMapping(value = "uploadPic")
    public Result uploadPic(@RequestParam("file") MultipartFile[] file, HttpServletRequest request) {
        List<FileBean> filePathList = fileService.upload(request, file);
        return Result.success(filePathList);
    }
}
