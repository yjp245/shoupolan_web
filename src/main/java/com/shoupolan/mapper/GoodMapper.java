package com.shoupolan.mapper;

import com.shoupolan.pojo.Domain;
import com.shoupolan.pojo.Good;
import com.shoupolan.pojo.GoodExample;
import com.shoupolan.pojo.dto.GoodDto;
import com.shoupolan.pojo.vo.DomainVo;
import com.shoupolan.pojo.vo.GoodVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GoodMapper {

    List<GoodDto> listPage(GoodVo goodVo);

    GoodDto getGoodInfo(String id);
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sp_good
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sp_good
     *
     * @mbg.generated
     */
    int insert(Good record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sp_good
     *
     * @mbg.generated
     */
    int insertSelective(Good record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sp_good
     *
     * @mbg.generated
     */
    List<Good> selectByExample(GoodExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sp_good
     *
     * @mbg.generated
     */
    Good selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sp_good
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(Good record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sp_good
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(Good record);
}