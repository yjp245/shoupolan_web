package com.shoupolan.controller;

import com.shoupolan.common.core.Result;
import com.shoupolan.pojo.Order;
import com.shoupolan.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName:
 * @PackageName: com.shoupolan.controller
 * @author: youjp
 * @create: 2019-08-16 21:10
 * @description:    TODO 订单控制层
 * @Version: 1.0
 */
@Controller
@RequestMapping("order")
public class OrderController {

    @Autowired
    OrderService orderService;

    /**
     * @Param [order]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 添加订单
     * @throw
     **/
    @ResponseBody
    @PostMapping("add")
    public Result add(@RequestBody Order order){
        orderService.add(order);
        return Result.success();
    }

    /**
     * @Param [id]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 删除订单
     * @throw
     **/
    @ResponseBody
    @DeleteMapping("delete")
    public Result delete(@RequestParam("id") String id){
        orderService.delete(id);
        return Result.success();
    }

    /**
     * @Param [id]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 获取订单详情
     * @throw
     **/
    @ResponseBody
    @GetMapping("/get")
    public Result get(@RequestParam("id") String id){
        return Result.success(orderService.get(id));
    }
}
