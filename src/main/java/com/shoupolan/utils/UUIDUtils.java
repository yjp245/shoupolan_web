package com.shoupolan.utils;

import java.util.UUID;

/**
 * @ClassName:
 * @PackageName: com.shoupolan.utils
 * @author: youjp
 * @create: 2019-08-16 21:27
 * @description:
 * @Version: 1.0
 */
public final class UUIDUtils {
    private UUIDUtils() {
    }

    public static String generatorSimple() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replaceAll("-", "");
    }

    public static String generator() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
