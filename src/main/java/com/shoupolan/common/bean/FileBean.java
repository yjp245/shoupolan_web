package com.shoupolan.common.bean;

/**
 * @ClassName:
 * @PackageName: com.shoupolan.common
 * @author: youjp
 * @create: 2019-08-18 18:16
 * @description:
 * @Version: 1.0
 */
public class FileBean {

    private String serveFileName;

    private String originalFileName;

    private String url;

    private String type;

    public FileBean(String serveFileName, String originalFileName, String url, String type) {
        this.serveFileName = serveFileName;
        this.originalFileName = originalFileName;
        this.url = url;
        this.type = type;
    }

    public FileBean() {
    }

    public String getServeFileName() {
        return serveFileName;
    }

    public void setServeFileName(String serveFileName) {
        this.serveFileName = serveFileName;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
