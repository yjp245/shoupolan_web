package com.shoupolan.utils;

import org.springframework.util.StringUtils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @program: demo
 * @description: 自定义工具类
 * @create: 2019-08-14 21:21
 */
public class PwdUtil {


    public static String SHA(String pwd) {
        if (StringUtils.isEmpty(pwd)) return null;
        String shaPawd = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(pwd.getBytes());
            shaPawd = new BigInteger(md.digest()).toString(32);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return shaPawd;
    }

}
