package com.shoupolan.controller;

import com.shoupolan.common.core.Result;
import com.shoupolan.pojo.Domain;
import com.shoupolan.pojo.vo.DomainVo;
import com.shoupolan.service.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName:
 * @PackageName: com.shoupolan.controller
 * @author: youjp
 * @create: 2019-08-16 21:08
 * @description:    TODO 域名控制层
 * @Version: 1.0
 */
@Controller
@RequestMapping("domain")
public class DomainController {

    @Autowired
    DomainService domainService;

    /**
     * @Param []
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 获取所有域名
     * @throw
     **/
   @GetMapping("/list")
   @ResponseBody
    public Result listDomain(){
       return Result.success(domainService.list()) ;
    }

    /**
     * @Param []
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 分页获取所有域名
     * @throw
     **/
    @GetMapping("/Page")
    @ResponseBody
    public Result listPageDomain(@ModelAttribute DomainVo addVo){
        return Result.success(domainService.listPage(addVo)) ;
    }


    /**
     * @Param [domain]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 添加域名
     * @throw
     **/
   @PostMapping("/add")
    @ResponseBody
    public Result create(@RequestBody Domain domain){
       domainService.add(domain);
       return Result.success() ;
    }

    /**
     * @Param [domain]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 修改域名
     * @throw
     **/
    @PutMapping("/edit")
    @ResponseBody
    public Result edit(@RequestBody Domain domain){
       domainService.update(domain);
        return Result.success() ;
    }


    /**
     * @Param [id]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 删除域名 By Id
     * @throw
     **/
    @DeleteMapping("/delete")
    @ResponseBody
    public Result delete(@RequestParam("id") String id){
        domainService.delete(id);
        return Result.success() ;
    }

    /**
     * @Param [id]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 获取域名 By Id
     * @throw
     **/
    @ResponseBody
    @GetMapping("/get")
    public Result get(@RequestParam("id") String id){
        return Result.success(domainService.get(id));
    }
}
