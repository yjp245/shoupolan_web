package com.shoupolan.controller;

import com.shoupolan.common.core.PageData;
import com.shoupolan.common.core.Result;
import com.shoupolan.error.BusinessException;
import com.shoupolan.pojo.User;
import com.shoupolan.pojo.vo.UserVo;
import com.shoupolan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import java.io.IOException;

/**
 * @ClassName:
 * @PackageName: com.shoupolan.controller
 * @author: youjp
 * @create: 2019-08-16 21:10
 * @description: TODO 用户控制层
 * @Version: 1.0
 */
@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    UserService userService;

    /**
     * @return com.shoupolan.common.core.Result
     * @Param [user]
     * @Author youjp
     * @Description //TODO 添加用户
     * @throw
     **/
    @ResponseBody
    @PutMapping("add")
    public Result add( User user) throws BusinessException {
        userService.add(user);
        return Result.success();
    }

    /**
     * @return com.shoupolan.common.core.Result
     * @Param [user]
     * @Author youjp
     * @Description //TODO 修改用户
     * @throw
     **/
    @ResponseBody
    @PutMapping("edit")
    public Result update(@RequestBody User user) {
        userService.update(user);
        return Result.success();
    }

    /**
     * @Param [id]
     * @return com.shoupolan.common.core.Result
     * @Author youjp
     * @Description //TODO 获取用户信息 By id
     * @throw
     **/
    @ResponseBody
    @GetMapping("/get")
    public Result get(@RequestParam("id") String id){
        return Result.success(userService.get(id));
    }

    /**
     * @return com.shoupolan.common.core.Result
     * @Param [id]
     * @Author youjp
     * @Description //TODO 删除用户
     * @throw
     **/
    @ResponseBody
    @DeleteMapping("delete")
    public Result delete(@RequestParam("id") String id) {
        userService.delete(id);
        return Result.success();
    }


    @PostMapping("/login")
    @ResponseBody
    @CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
    public Result login( User vo) {
        User user = userService.validateLogin(vo);
        if (user == null)
            return Result.fail();
        else
            return Result.success();
    }

    @GetMapping("/page")
    @ResponseBody
    @CrossOrigin(allowCredentials = "true", allowedHeaders = "*")
    public Result page( UserVo vo) {
         return Result.success( userService.getPage(vo));
    }
}
