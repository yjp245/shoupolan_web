package com.shoupolan.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebViewConfig extends WebMvcConfigurerAdapter{

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		// TODO Auto-generated method stub
	     registry.addViewController("/").setViewName("forward:/index");
	        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	        super.addViewControllers(registry);

	}
}
