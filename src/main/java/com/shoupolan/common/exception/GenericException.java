package com.shoupolan.common.exception;

/**
 * @ClassName:
 * @PackageName: com.shoupolan.common.exception
 * @author: youjp
 * @create: 2019-08-14 22:53
 * @description:
 * @Version: 1.0
 */
public class GenericException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private int code;
    private String msg;

    public GenericException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public GenericException(String message, int code, String msg) {
        super(message);
        this.code = code;
        this.msg = msg;
    }

    public GenericException(String message) {
        super(message);
    }

    public GenericException(String message, Throwable cause, int code, String msg) {
        super(message, cause);
        this.code = code;
        this.msg = msg;
    }

    public GenericException(String message, Throwable cause) {
        super(message, cause);
    }

    public GenericException(Throwable cause, int code, String msg) {
        super(cause);
        this.code = code;
        this.msg = msg;
    }

    public GenericException(Throwable cause) {
        super(cause);
    }

    public GenericException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, int code, String msg) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = code;
        this.msg = msg;
    }

    public GenericException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
